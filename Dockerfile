
FROM exiasr/alpine-yarn-nginx

WORKDIR /tmp/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

COPY webpack.config.js ./

COPY ./config ./config

RUN yarn

COPY . .

RUN yarn build
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source

RUN pwd

RUN ls

RUN cp -a /tmp/app/dist/. /usr/share/nginx/html/
#COPY /tmp/app/dist /usr/share/nginx/html/

EXPOSE 80
