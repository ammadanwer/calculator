import React, {Component} from 'react';
import Button from 'material-ui/Button';
import Drawer from 'material-ui/Drawer';
import {connect} from 'react-redux'
import {CircularProgress} from 'material-ui/Progress';
import 'jquery-slimscroll/jquery.slimscroll.min';
import IconButton from 'material-ui/IconButton'
import Input from 'material-ui/Input'
import Moment from 'moment';
import ChatUserList from 'components/chatPanel/ChatUserList';
import Conversation from 'components/chatPanel/Conversation/index';
import users from '../data/calculators_list';
import ContactList from 'components/chatPanel/ContactList/index';
import SearchBox from 'components/SearchBox/index';
import createHistory from 'history/createBrowserHistory'
import Calculator from 'components/calculators/simpleCalculator';
import FractoPercentage from 'components/calculators/fractopercentage';
import PerctoFraction from 'components/calculators/perctofraction';
import SimplePercentage from 'components/calculators/simplepercentage';
import PercentageChange from 'components/calculators/percentagechange';
import PercentageDecrease from 'components/calculators/percentagedecrease';
import PercentageDifference from 'components/calculators/percentagedifference';
import PercentageOfPercentage from 'components/calculators/percentageofpercentage';
import PercentError from 'components/calculators/percenterror';

class Calculators extends Component {
    filterContact = (userName) => {
        if (userName === '') {
            return users.filter(user => user.sub_cat_id == this.state.subcat);
        }
        return users.filter((user) =>
            user.name.toLowerCase().indexOf(userName.toLowerCase()) > -1 && user.sub_cat_id == this.state.subcat
        );
    };
    filterUsers = (userName) => {
        if (userName === '') {
            return users.filter(user => user.recent);
        }
        return users.filter((user) =>
            user.recent && user.name.toLowerCase().indexOf(userName.toLowerCase()) > -1
        );
    };
    getCalculatorByTag = (selectedUser) => {
      console.log("selectedUser");
      console.log(selectedUser);
      var calc = "";
      if(selectedUser.tag_name == 'Calculator'){
        calc = <Calculator/>
      }
      else if(selectedUser.tag_name == 'FractoPerc'){
        calc = <FractoPercentage/>
      }
      else if(selectedUser.tag_name == 'PerctoFrac'){
        calc = <PerctoFraction/>
      }
      else if(selectedUser.tag_name == 'SimplePercentage'){
        calc = <SimplePercentage/>
      }
      else if(selectedUser.tag_name == 'Percentage-Change'){
        calc = <PercentageChange/>
      }
      else if(selectedUser.tag_name == 'Percentage-Decrease'){
        calc = <PercentageDecrease/>
      }
      else if(selectedUser.tag_name == 'Percentage-Difference'){
        calc = <PercentageDifference/>
      }
      else if(selectedUser.tag_name == 'PercentageOfPercentage'){
        calc = <PercentageOfPercentage/>
      }
      else if(selectedUser.tag_name == 'PercentError'){
        calc = <PercentError/>
      }
      
      else {
        calc = <div className="loader-view">
            <i className="zmdi zmdi-comment s-128 text-muted"/>
            <h1 className="text-muted"> Select Calculator</h1>
        </div>
      }
      return calc;
    };

    Communication = () => {
        const {message, selectedUser, conversation} = this.state;
        //const {conversationData} = conversation;
        var calc = this.getCalculatorByTag(selectedUser);
        console.log(calc);
        return <div className="chat-main">
            <div className="chat-main-header">
                <IconButton className="d-block d-xl-none chat-btn" aria-label="Menu"
                            onClick={this.onToggleDrawer.bind(this)}>
                    <i className="zmdi zmdi-comment-text"/>
                </IconButton>
                <div className="chat-main-header-info">

                    <div className="chat-avatar mr-2">
                        <div className="chat-avatar-mode">
                            <img src={selectedUser.thumb}
                                 className="rounded-circle size-60"
                                 alt=""/>
                                 <span className={`chat-mode ${selectedUser.status}`}/>
                         </div>
                       </div>
                       <div className="chat-contact-name">
                        {selectedUser.name}
                      </div>
                    </div>
                  </div>
                  <div className="jr-card">
                    {calc}
                  </div>
                </div>
    };

    AppUsersInfo = () => {
        return <div className="chat-sidenav-main">
            <div className=" bg-primary chat-sidenav-header">

                <div className="chat-user-hd">
                    <IconButton className="back-to-chats-button" aria-label="back button"
                                onClick={() => {
                                    this.setState({
                                        userState: 1
                                    });
                                }}>
                        <i className="zmdi zmdi-arrow-back text-white"/>
                    </IconButton>
                </div>
                <div className="chat-user chat-user-center">
                    <div className="chat-avatar">
                        <img src="http://via.placeholder.com/256x256"
                             className="avatar rounded-circle size-60 huge" alt="John Doe"/>
                    </div>

                    <div className="user-name h4 my-2 text-white">Robert Johnson</div>

                </div>
            </div>
            <div className="chat-sidenav-content">

                <div className="chat-sidenav-scroll card p-4">
                    <form>
                        <div className="form-group mt-4">
                            <label>Mood</label>

                            <Input
                                fullWidth
                                id="exampleTextarea"
                                multiline
                                rows={3}
                                onKeyUp={this._handleKeyPress.bind(this)}
                                onChange={this.updateMessageValue.bind(this)}
                                defaultValue="it's a status....not your diary..."
                                placeholder="Status"
                                margin="normal"/>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    };
    ChatUsers = () => {
        return <div className="chat-sidenav-main">
            <div className="chat-sidenav-header">

                <div className="chat-user-hd">
                    {this.props.category}
                </div>

                <div className="search-wrapper">

                    <SearchBox placeholder="Search Calculator"
                               onChange={this.updateSearchChatUser.bind(this)}
                               value={this.state.searchChatUser}/>

                </div>
            </div>

            <div className="chat-sidenav-content">
                <div className="chat-sidenav-scroll">
                    {this.state.contactList.length === 0 ?
                        <div className="pl-3">{this.state.userNotFound}</div>
                        :
                        <ContactList contactList={this.state.contactList}
                                     selectedSectionId={this.state.selectedSectionId}
                                     onSelectUser={this.onSelectUser.bind(this)}/>
                    }
                </div>
            </div>
        </div>
    };
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.submitComment();
        }
    };

    onSelectUser = (user) => {
        this.setState({
            loader: true,
            selectedSectionId: user.id,
            drawerState: this.props.drawerState,
            selectedUser: user,
        });
        setTimeout(() => {
            this.setState({loader: false});
        }, 100);
    };
    showCommunication = () => {
        return (
            <div className="chat-box">
                <div className="chat-box-main">{
                    this.state.selectedUser === null ?
                        <div className="loader-view">
                            <i className="zmdi zmdi-comment s-128 text-muted"/>
                            <h1 className="text-muted"> Select Calculator</h1>
                            <Button className="d-block d-xl-none" color="primary"
                                    onClick={this.onToggleDrawer.bind(this)}>Select calculator
                                </Button>
                        </div>
                        : this.Communication()}
                </div>
            </div>)
    };

    constructor(props) {
        super(props);
        if(props.subcat)
        {
          var suser = props.subcalc ? users.filter((user) => user.tag_name == props.subcalc && user.sub_cat_id == props.subcat)[0] : null;
          var clist = users.filter((user) => user.sub_cat_id == props.subcat);
        }
        else
        {
          var suser = null;
          var clist = users.filter((user) => user.cat_id == props.cat_id);
        }
        const history = createHistory();
        this.state = {
            loader: false,
            userNotFound: 'No Calculator Found',
            drawerState: false,
            selectedSectionId: '',
            userState: 1,
            searchChatUser: '',
            contactList: clist,
            selectedUser: suser,
            message: '',
            chatUsers: users.filter((user) => user.recent),
            conversation: null,
            cat: props.cat_id,
            subcat: props.subcat,
            history : history
        }
    }
    componentWillReceiveProps(props)
    {
      if(props.subcat)
      {
        var suser = props.subcalc ? users.filter((user) => user.tag_name == props.subcalc && user.sub_cat_id == props.subcat)[0] : null;
        var clist = users.filter((user) => user.sub_cat_id == props.subcat);
      }
      else
      {
        var suser = null;
        var clist = users.filter((user) => user.cat_id == props.cat_id);
      }
      const history = createHistory();
      this.state = {
          loader: false,
          userNotFound: 'No Calculator Found',
          drawerState: false,
          selectedSectionId: '',
          userState: 1,
          searchChatUser: '',
          contactList: clist,
          selectedUser: suser,
          message: '',
          chatUsers: users.filter((user) => user.recent),
          conversation: null,
          cat: props.cat_id,
          subcat: props.subcat,
          history : history
      }
    }

    componentDidUpdate() {
        this.manageHeight()
    }

    componentDidMount() {
        this.manageHeight()
    }

    manageHeight() {
        if (this.props.width >= 1200) {
            $('.loader-view').slimscroll({
                height: 'calc(100vh - 290px)'
            });
            $('.chat-list-scroll').slimscroll({
                height: 'calc(100vh - 346px)'
            });

            if (this.state.userState === 1) {
                $('.chat-sidenav-scroll').slimscroll({
                    height: 'calc(100vh - 297px)'
                });
            } else {
                $('.chat-sidenav-scroll').slimscroll({
                    height: 'calc(100vh - 361px)'
                });
            }
        } else {
            $('.loader-view').slimscroll({
                height: 'calc(100vh - 122px)'
            });
            $('.chat-list-scroll').slimscroll({
                height: 'calc(100vh - 255px)'
            });

            if (this.state.userState === 1) {
                $('.chat-sidenav-scroll').slimscroll({
                    height: 'calc(100vh - 127px)'
                });
            } else {
                $('.chat-sidenav-scroll').slimscroll({
                    height: 'calc(100vh - 187px)'
                });
            }
        }
    }

    submitComment() {
        if (this.state.message !== '') {
            const updatedConversation = this.state.conversation.conversationData.concat({
                'type': 'sent',
                'message': this.state.message,
                'sentAt': Moment(new Date).format('ddd DD, YYYY, hh:mm:ss A'),
            });
        }
    }

    updateMessageValue(evt) {
        this.setState({
            message: evt.target.value
        });
    }

    updateSearchChatUser(evt) {
        this.setState({
            searchChatUser: evt.target.value,
            contactList: this.filterContact(evt.target.value),
            chatUsers: this.filterUsers(evt.target.value)
        });
    }

    onToggleDrawer() {
        this.setState({
            drawerState: !this.state.drawerState
        });
    }

    render() {
        const {loader, userState, drawerState, selectedUser} = this.state;
        return (
            <div className="app-wrapper app-wrapper-module">
                <div className="app-module chat-module animated slideInUpTiny animation-duration-3">
                    <div className="chat-module-box">
                        <div className="d-block d-xl-none">
                            <Drawer className="app-module-sidenav"
                                    type="temporary"
                                    open={drawerState}
                                    onClose={this.onToggleDrawer.bind(this)}>
                                {userState === 1 ? this.ChatUsers() : this.AppUsersInfo()}
                            </Drawer>
                        </div>
                        <div className="chat-sidenav d-none d-xl-flex">
                            {userState === 1 ? this.ChatUsers() : this.AppUsersInfo()}
                        </div>
                        {this.showCommunication()
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({settings}) => {
    const {width} = settings;
    return {width}
};

export default connect(mapStateToProps)(Calculators);
