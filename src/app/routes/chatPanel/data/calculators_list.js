export default [
    {
        id: 1,
        name: 'Simple Calculator',
        tag_name : 'Calculator',
        cat_id : 'maths',
        thumb: 'http://via.placeholder.com/256x256',
        status: 'away',
        mood: 'English versions from the 1914 translation by H. Rackham',
        lastMessage: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem',
        unreadMessage: '',
        lastMessageTime: '20/11/17',
        recent: false
    }, {
        id: 2,
        name: 'Fraction to Percentage',
        tag_name : 'FractoPerc',
        cat_id : 'maths',
        thumb: 'http://via.placeholder.com/256x256',
        status: 'online',
        mood: 'English versions from the 1914 translation by H. Rackham',
        lastMessage: 'It is a long established fact',
        unreadMessage: '4',
        lastMessageTime: '20/11/17',
        recent: true
    }, {
        id: 3,
        name: 'Acceleration',
        tag_name : 'Acceleration',
        cat_id : 'physics',
        thumb: 'http://via.placeholder.com/256x256',
        status: 'online',
        mood: 'English versions from the 1914 translation by H. Rackham',
        lastMessage: 'It is a long established fact',
        unreadMessage: '4',
        lastMessageTime: '20/11/17',
        recent: true
    }
]
