import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';
class PercentageChange extends Component {
  state = {
      initial: '',
      final: '',
      change: '',
      difference: '',
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.initial != '' && this.state.final != '')
        {
          this.setState({
              change: eval(((this.state.final-this.state.initial)* 100/ this.state.initial)).toFixed(5) + '%',
          }, () => {console.log(((this.state.final-this.state.initial)* 100/ this.state.initial).toFixed(5));});
          this.setState({
              difference: eval(this.state.final-this.state.initial),
          }, () => {console.log(this.state.final-this.state.initial)});
        }
        else if (this.state.initial != '' || this.state.final != '') {

        }
        else {
          this.setState({
              change: '',
          }, () => {console.log(this.state.change);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        initial: '',
        final: '',
        change: '',
        difference: ''
    });
  }
  render() {
    return (
      <div className="FractoPercentage" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.initial}
                  label="Initial Value"
                  onChange={this.handleChange('initial').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
              <div className="col-md-3 col-12">

              <TextField
                  value={this.state.final}
                  label="Final Value"
                  onChange={this.handleChange('final').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.change}
              label="Change"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.difference}
              label="Difference"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default PercentageChange;
