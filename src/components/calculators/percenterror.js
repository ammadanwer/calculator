import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';
class PercentError extends Component {
  state = {
      TrueVal: '',
      ObservedVal: '',
      PercError: ''
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(name == 'TrueVal')
        {
          if(this.state.TrueVal != '' && this.state.ObservedVal != '')
          {
            this.setState({'PercError': eval((this.state.TrueVal - this.state.ObservedVal)/this.state.TrueVal * 100)});

          }
          else if(this.state.TrueVal != '' && this.state.PercError != '')
          {
            this.setState({'ObservedVal': eval(-((this.state.PercError * this.state.TrueVal/ 100) + this.state.TrueVal))});

          }
        }
        else if(name == 'ObservedVal')
        {
          if(this.state.ObservedVal != '' && this.state.TrueVal != '')
          {
            this.setState({'PercError': eval((this.state.TrueVal - this.state.ObservedVal)/this.state.TrueVal * 100)});

          }
          else if(this.state.ObservedVal != '' && this.state.PercError != '')
          {
            this.setState({'TrueVal': eval((this.state.PercError / 100) - this.state.ObservedVal)});

          }
        }
        else if(name == 'PercError')
        {
          if(this.state.PercError != '' && this.state.ObservedVal != '')
          {
            this.setState({'TrueVal': eval((this.state.PercError / 100) - this.state.ObservedVal)});

          }
          else if(this.state.PercError != '' && this.state.TrueVal != '')
          {
            this.setState({'ObservedVal': eval((this.state.PercError / 100) + this.state.TrueVal)});

          }
        }

      });

  }
  clearInput = () => {
    this.setState({
      TrueVal: '',
      ObservedVal: '',
      PercError: '',
    });
  }
  render() {
    return (
      <div className="PercentError" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.TrueVal}
                  label="True Value"
                  onChange={this.handleChange('TrueVal').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
              <div className="col-md-3 col-12">

              <TextField
                  value={this.state.ObservedVal}
                  label="Observed Value"
                  onChange={this.handleChange('ObservedVal').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
          <div className="col-md-3 col-12">

              <TextField
                  value={this.state.PercError}
                  label="Percent Error"
                  onChange={this.handleChange('PercError').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default PercentError;
