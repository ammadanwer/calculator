import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';

class PerctoFraction extends Component {
  state = {
      input: '',
      result: '',
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.input != '' )
        {
          var fraction = math.fraction(this.state.input);
          this.setState({
              result: 'is same as ' + fraction.n + '/' + fraction.d,
          }, () => {console.log(this.state.result);});
        }
        else {
          this.setState({
              result: '',
          }, () => {console.log(this.state.result);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        input: '',
        result:''
    });
  }
  render() {
    return (
      <div className="PerctoFraction" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.input}
                  label="Percentage"
                  onChange={this.handleChange('input').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.result}
              label="Result"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default PerctoFraction;
