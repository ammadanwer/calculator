import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';
class FractoPercentage extends Component {
  state = {
      numerator: '',
      denominator: '',
      result: '',
      multiline: 'Controlled',
      currency: 'EUR',
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.numerator != '' && this.state.denominator != '')
        {
          this.setState({
              result: 'is same as ' + eval(this.state.numerator* 100/this.state.denominator) + '%',
          }, () => {console.log(math.fraction(eval(this.state.numerator/this.state.denominator)));});
        }
        else if (this.state.numerator != '' || this.state.denominator != '') {

        }
        else {
          this.setState({
              result: '',
          }, () => {console.log(this.state.result);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        numerator: '',
        denominator: '',
        result: ''
    });
  }
  render() {
    return (
      <div className="FractoPercentage" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.numerator}
                  label="Numerator"
                  onChange={this.handleChange('numerator').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
              <div className="col-md-3 col-12">

              <TextField
                  value={this.state.denominator}
                  label="Denominator"
                  onChange={this.handleChange('denominator').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.result}
              label="Result"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default FractoPercentage;
