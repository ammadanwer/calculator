import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';
class PercentageDifference extends Component {
  state = {
      valueA: '',
      valueB: '',
      result: '',
      difference: '',
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.valueA != '' && this.state.valueB != '')
        {
          var avg = eval(this.state.valueA/2 + this.state.valueB/2);
          this.setState({
              result: eval(Math.abs(this.state.valueA - this.state.valueB)*100/avg) + '%',
          }, () => {
            this.setState({
            difference: eval(Math.abs(this.state.valueA - this.state.valueB))});
            console.log(Math.abs(this.state.valueA - this.state.valueB));
          });
        }
        else if (this.state.pdecrease != '' || this.state.base != '') {

        }
        else {
          this.setState({
              result: '',
              difference: '',
          }, () => {console.log(this.state.result);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        valueA: '',
        valueB: '',
        result: '',
        difference: ''
    });
  }
  render() {
    return (
      <div className="PercentageDifference" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.valueA}
                  label="value A"
                  onChange={this.handleChange('valueA').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
              <div className="col-md-3 col-12">

              <TextField
                  value={this.state.valueB}
                  label="value B"
                  onChange={this.handleChange('valueB').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.result}
              label="is"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.difference}
              label="Difference"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default PercentageDifference;
