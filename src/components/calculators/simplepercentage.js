import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';

class SimplePercentage extends Component {
  state = {
    percentage: '',
    base: '',
    result:''
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.percentage != '' && this.state.base != '')
        {
          this.setState({
              result: 'is ' + eval((this.state.percentage*this.state.base)/100),
          }, () => {console.log(this.state.result);});
        }
        else {
          this.setState({
              result: '',
          }, () => {console.log(this.state.result);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        percentage: '',
        base: '',
        result:''
    });
  }
  render() {
    return (
      <div className="SimplePercentage" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.percentage}
                  label="Percentage"
                  onChange={this.handleChange('percentage').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
          </div>
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.base}
                  label="Base"
                  onChange={this.handleChange('base').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.result}
              label="Result"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default SimplePercentage;
