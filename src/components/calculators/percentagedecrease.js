import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import TextFields from '../../app/routes/components/routes/textFields/textField/TextFields';
import './../../styles/fractopercentage.scss';
import Button from 'material-ui/Button';
class PercentageDecrease extends Component {
  state = {
      pdecrease: '',
      base: '',
      result: '',
      difference: '',
      multiline: 'Controlled',
  };
  handleChange = name => event => {

      var math = require('mathjs');
      this.setState({
          [name]: event.target.value,
      },()=>{
        if(this.state.pdecrease != '' && this.state.base != '')
        {
          this.setState({
              result: eval((100 - this.state.pdecrease)*this.state.base/100),
          }, () => {
            this.setState({
            difference: eval(this.state.result - this.state.base)});
            console.log((100 - this.state.pdecrease)*this.state.base/100);
          });
        }
        else if (this.state.pdecrease != '' || this.state.base != '') {

        }
        else {
          this.setState({
              result: '',
              difference: '',
          }, () => {console.log(this.state.result);});
        }

      });

  }
  clearInput = () => {
    this.setState({
        pdecrease: '',
        base: '',
        result: '',
        difference: ''
    });
  }
  render() {
    return (
      <div className="PercentageDecrease" >
        <div className = "row">
          <div className="col-md-3 col-12">
              <TextField
                  value={this.state.pdecrease}
                  label="% decrease"
                  onChange={this.handleChange('pdecrease').bind(this)}
                  type="number"
                  margin="normal"
                  fullWidth
              />
              </div>
              <div className="col-md-3 col-12">

              <TextField
                  value={this.state.base}
                  label="from"
                  onChange={this.handleChange('base').bind(this)}
                  margin="normal"
                  type="number"
                  fullWidth
              />

          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.result}
              label="is"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
          <div className="col-md-3 col-12">
          <TextField
              value={this.state.difference}
              label="Difference"
              margin="normal"
              disabled="true"
              fullWidth
          />
          </div>
        </div>
        <Button variant="fab" className="jr-fab-btn bg-white" onClick={this.clearInput.bind(this)}>
            <i className="zmdi zmdi-refresh zmdi-hc-fw"/>
        </Button>
      </div>
    )
  }
}
export default PercentageDecrease;
