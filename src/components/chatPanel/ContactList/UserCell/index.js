import React from 'react';
import {Link} from 'react-router-dom';
const UserCell = ({onSelectUser, selectedSectionId, user}) => {
  var link = window.location.href.indexOf(user.sub_cat_id +'/') >= 0 ? user.tag_name : window.location.href.indexOf(user.cat_id +'/') ?
  user.sub_cat_id+ '/' + user.tag_name : ser.cat_id +'/'+ user.sub_cat_id+ '/' + user.tag_name;
  console.log('abc: '+window.location.href);
    return (
      <Link to={link}>
        <div className={`chat-user-item ${selectedSectionId === user.id ? 'active' : ''}`} onClick={() => {
            onSelectUser(user);
        }}>
            <div className="chat-user-row row">
                <div className="chat-avatar col-xl-2 col-3">
                    <div className="chat-avatar-mode">
                        <img src={user.thumb} className="rounded-circle size-40" alt="Abbott"/>
                        <span className={`chat-mode smallcal ${user.status}`}/>
                    </div>
                </div>

                <div className="chat-contact-col col-xl-10 col-9">
                    <div className="h4 name">{user.name}</div>
                </div>
            </div>
        </div>
        </Link>
    )
};

export default UserCell;
